﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

	private Text scoreText;
	private GameObject victoryText;
	private GameObject defeatText;
	private int score = 0;
	private int bonusCount;

	private void Victory() {
		victoryText.SetActive (true);
	}

	private void Defeat() {
		defeatText.SetActive (true);
	}

	private void UpdateScoreText() {
		scoreText.text = score + "/" + bonusCount + " Bonus";
	}

	void Start() {
		scoreText = GameObject.Find ("Hud/ScoreText").GetComponent<Text>();
		victoryText = GameObject.Find ("Hud/VictoryText");
		defeatText = GameObject.Find ("Hud/DefeatText");
		bonusCount = GameObject.FindGameObjectsWithTag ("Bonus").Length;
		victoryText.SetActive (false);
		defeatText.SetActive (false);
		UpdateScoreText ();
	}

	void Update() {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			SceneManager.LoadScene ("MenuScene");
		}
		GameObject sphere = GameObject.Find ("Sphere");
		if (sphere != null && sphere.transform.position.y < -16) {
			OnPlayerFall ();
		}
	}

	public void OnBonusPickup() {
		score++;
		UpdateScoreText ();
		if (score >= bonusCount) {
			Victory ();
		}
	}

	public void OnMalusPickup() {
		Defeat ();
	}

	public void OnPlayerFall() {
		Defeat ();
	}
}
