﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MalusController : MonoBehaviour {

	private GameController gameController;

	void Start() {
		gameController = GameObject.Find ("GameManager").GetComponent<GameController> ();
	}

	void OnCollisionEnter(Collision collision) {
		Destroy (collision.gameObject);
		gameController.OnMalusPickup ();
	}
}

