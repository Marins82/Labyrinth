﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupAnimator : MonoBehaviour {

	private Vector3 startPosition;
	public float frequency = 1;
	public float offset = 0;
	public float amplitude = 1;
	public float speed = 1;

	void Start () {
		startPosition = transform.localPosition;
	}

	void Update () {
		transform.localPosition = startPosition + new Vector3(0, Mathf.Cos (Time.fixedTime * frequency + offset) * amplitude, 0);
		transform.Rotate (0, Time.deltaTime * speed, 0);
	}
}
