﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusController : MonoBehaviour {

	private GameController gameController;

	void Start() {
		gameController = GameObject.Find ("GameManager").GetComponent<GameController> ();
	}

	void OnCollisionEnter(Collision collision) {
		Destroy (transform.gameObject);
		gameController.OnBonusPickup ();
	}
}
